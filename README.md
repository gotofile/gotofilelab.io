# gotofile.gitlab.io

GoTo files website.

## CLI Commands

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production in /public/ (note: assumes that the base URL is /)
npm run build
```
