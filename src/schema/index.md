---
layout: layout.njk
title: Schema
description: Learn more about the GoTo file schema.
---

GoTo files are JSON files that follow a particular schema.

The schema definition is located at <https://gotofile.gitlab.io/gotofile/goto.schema.json>.  
The schema development repository is [on GitLab](https://gitlab.com/gotofile/gotofile).

## File

The file extension for GoTo files is `.goto`.

It is recommended to minify the JSON string that is saved as a `.goto` file.

## Keys

### `version`

The `version` key specifies the version of the GoTo file schema used by the file.

This key is **required**, and its value must be **the number `1`**.

### `url`

The `url` key contains the URL that should be opened when the file is opened.

This key is **required**, and it must be a **valid URL**.

### `notes`

The `notes` key is used to keep notes that the user has inserted.

This key is **optional**, and it must be a **string**.