---
layout: layout.njk
title: Privacy - GoTo files for Google Drive
description: Privacy information for the GoTo files Google Drive addon.
menu: gdrive
---

GoTo files for Drive is an [open-source](https://gitlab.com/gotofile/gotofile-gdrive) and client-side (all processing is done on your device) application.  
File contents and/or metadata are transmitted directly from your device to Google Drive, and viceversa, over a secure connection. File contents and/or metadata are not transmitted elsewhere by the application.  
The application is hosted on [GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/). Access to it may be subject to [GitLab's privacy policy](https://about.gitlab.com/privacy/).

The conditions described above may be subject to change.