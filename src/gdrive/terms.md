---
layout: layout.njk
title: Terms - GoTo files for Google Drive
description: Terms of use for the GoTo files Google Drive addon.
menu: gdrive
---

GoTo files for Drive is an [open-source](https://gitlab.com/gotofile/gotofile-gdrive) and client-side (all processing is done on your device) application.  
The application is hosted on [GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/). Access to it may be subject to [GitLab's terms of use](https://about.gitlab.com/terms/).

The conditions described above may be subject to change.