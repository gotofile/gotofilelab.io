---
layout: layout.njk
title: Privacy - GoTo maker
description: Privacy information for the GoTo maker extension.
menu: maker
---
This add-on, by itself, never sends data outside the device.

The only data collected is URLs used to create the files that are then saved in your chosen location.

## Permission breakdown

### `tabs` (sometimes displayed as "Access browser tabs" or "Read your browsing history")

This permission is requested to get access to the URL you selected.

Where possible, the creation of files from tabs different than the current one is supported, therefore the `tabs` permission is used instead of `activeTab`.

This add-on never reads your full browser history.

### `downloads` (sometimes displayed as "Download files and read and modify the browser's download history" or "Manage your downloads")

This permission is requested to be able to save the file in the user's preferred location and to ask the user what that location is.

The download history is never read or modified (except for the addition of the saved GoTo file)

### `contextMenus`

This permission is requested to add "Create GoTo file" actions to the browser's context menu.