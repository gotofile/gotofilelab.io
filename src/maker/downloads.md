---
layout: layout.njk
title: Install - GoTo maker
description: Install the GoTo maker add-on to create GoTo files directly from your browser.
menu: maker
---
GoTo maker is available from:

- [Mozilla add-ons](https://addons.mozilla.org/firefox/addon/gotomake/?src=external-website-downloads)
- [Chrome Web Store](https://chrome.google.com/webstore/detail/goto-maker/ckaeijlplnplohimhbocpaphilmcghfk?utm_source=website&utm_medium=downloads-page&utm_campaign=download-link)
- [Microsoft Edge Addons](https://microsoftedge.microsoft.com/addons/detail/kjpjgmdiadfbokfgeafchjgolfcnbhif)