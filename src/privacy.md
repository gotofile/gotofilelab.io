---
layout: layout.njk
title: Privacy info
description: Privacy information for the GoTo files website.
---

# Privacy info

This website is hosted on [GitLab
Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/). Access may be
subject to [GitLab's privacy policy](https://about.gitlab.com/privacy/).

Some resources are loaded from [cdnjs](https://cdnjs.com/).

This site uses [Plausible Analytics](https://plausible.io/).  
[Plausible Analytics data policy](https://plausible.io/data-policy)