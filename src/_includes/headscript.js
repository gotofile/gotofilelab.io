window.addEventListener('load', function() {
  var modalVersion = '2021.0';

  function openCookieModal(force) {
    // check if the version of the modal that the user viewed is the current one
    var userModalVersion = localStorage.getItem('gotofile-cookieinfo');
    if (!force && (userModalVersion === modalVersion)) return;
    // open the modal
    $('#cookieModal').modal();
  }

  // the entire modal is a form
  $('#cookieModal').on('submit', function(e) {
    // when it is submitted, save the modal version and close
    e.preventDefault();
    localStorage.setItem('gotofile-cookieinfo', modalVersion);
    $('#cookieModal').modal('hide');
  });
  // open the modal without forcing (if a setting is saved the modal won't be opened)
  openCookieModal(false);
});