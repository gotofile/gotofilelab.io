---
layout: layout.njk
title: Privacy - GoTo editor (UWP)
description: Privacy information for the UWP version of GoTo editor.
menu: editor-uwp
---
This application collects the following data:
- URLs
- Notes (arbitrary user input into a field labelled "Notes")

The application only saves this data inside the files created or opened within
it. This application does not transmit data outside the device by itself.