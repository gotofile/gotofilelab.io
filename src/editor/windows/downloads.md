---
layout: layout.njk
title: Downloads - GoTo editor for Windows
description: Download the GoTo editor application on your Windows computer.
menu: editor-windows
---
# Requirements

GoTo editor for Windows requires .NET Framework 4.6.1.

If you have an older version of .NET Framework, you may download 4.6.1 from <https://www.microsoft.com/en-us/download/details.aspx?id=49981>.

# Releases

The latest releases can be found [on GitLab](https://gitlab.com/gotofile/gotoedit-windows/-/releases).

Each release contains a setup file, which can be found in the "Assets" section.  
The setup file is named `GoToEditor-x.x.x.x-Setup.exe`.