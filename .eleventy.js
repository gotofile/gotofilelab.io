const minify = require('html-minifier').minify;
const CleanCSS = require("clean-css");
const terser = require('terser');

module.exports = config => {
  config.setTemplateFormats([
    'md',
    'html',
    'njk',
    // the following extensions don't have templates and are just copied
    'txt',
    'json',
    'ico'
  ]);

  config.addNunjucksAsyncFilter('jsmin', async function(code, callback) {
    try {
      const minified = await terser.minify(code);
      callback(null, minified.code);
    } catch (err) {
      console.error('Terser error: ', err);
      callback(null, code);
    }
  });

  config.addFilter('cssmin', function(code) {
    return new CleanCSS({}).minify(code).styles;
  });

  config.addTransform('htmlmin', function(content, outputPath) {
    if( outputPath.endsWith(".html") ) {
      return minify(content, {
        useShortDoctype: true,
        removeComments: true,
        collapseWhitespace: true
      });
    } else {
      return content;
    }
  });

  return {
    dir: {
      input: 'src',
      output: 'public',
    },
  }
}